#include <iostream>
#include <stdlib.h> //necessario p/ funções rand() e srand()
#include <time.h> //necessario p/ função time() 
#include <ios> //usado para pegar o stream size
#include <sstream>
#include <limits> //usado para pegar limite numerico

int tentativas(int NumTentativa, int NumA, int NumB, int NumC, int AcertouA, int AcertouB, int AcertouC){
    static int NumA_Tent1, NumB_Tent1, NumC_Tent1;
    static int NumA_Tent2, NumB_Tent2, NumC_Tent2;
    static int NumA_Tent3, NumB_Tent3, NumC_Tent3;

    std::cout<< "\t \t \t \t \t \t \t \t| \t |--------------- Tentativas ---------------"<<std::endl;
    std::cout<< "\t \t \t \t \t \t \t \t| \t | Acertos: "<< AcertouA<<", "<<AcertouB<<", "<<AcertouC<<std::endl;
    if(NumTentativa==0){
        return 0;
    }else if(NumTentativa>=1){
        if(NumTentativa == 1){
            NumA_Tent1 = NumA;
            NumB_Tent1 = NumB;
            NumC_Tent1 = NumC;
        }
        std::cout<< "\t \t \t \t \t \t \t \t| \t |                                                       "<<std::endl;
        std::cout<< "\t \t \t \t \t \t \t \t| \t | 1.   Primeira Tentativa: "<<NumA_Tent1<<", "<<NumB_Tent1<<", "<<NumC_Tent1<<"."<< std::endl;
        std::cout<< "\t \t \t \t \t \t \t \t| \t |                                                       "<<std::endl;
        if(NumTentativa ==1){
            std::cout<< "\t \t \t \t \t \t \t \t| \t |------------------------------------------"<<std::endl;
        }
    } if(NumTentativa>=2){
        if(NumTentativa == 2){
            NumA_Tent2 = NumA;
            NumB_Tent2 = NumB;
            NumC_Tent2 = NumC;
        }
        std::cout<< "\t \t \t \t \t \t \t \t| \t | 2.   Segunda Tentativa: "<<NumA_Tent2<<", "<<NumB_Tent2<<", "<<NumC_Tent2<<"."<<std::endl;
        std::cout<< "\t \t \t \t \t \t \t \t| \t |                                                       "<<std::endl;
        if(NumTentativa ==2){
            std::cout<< "\t \t \t \t \t \t \t \t| \t |------------------------------------------"<<std::endl;
        }
    } if(NumTentativa==3){
        if(NumTentativa == 3){
            NumA_Tent3 = NumA;
            NumB_Tent3 = NumB;
            NumC_Tent3 = NumC;
        }
        std::cout<< "\t \t \t \t \t \t \t \t| \t | 3.   Terceira Tentativa: "<<NumA_Tent3<<", "<<NumB_Tent3<<", "<<NumC_Tent3<<"."<<std::endl;
        std::cout<< "\t \t \t \t \t \t \t \t| \t |                                                       "<<std::endl;
        if(NumTentativa ==3){
            std::cout<< "\t \t \t \t \t \t \t \t| \t |------------------------------------------"<<std::endl;
        }
    }
    

    return 0;
}

void image (){
    std::cout<< "\t \t \t \t \t \t \t \t    '\n";
    std::cout<< "\t \t \t \t \t \t \t \t''---.  .-.   .--,;._\n";
    std::cout<< "\t \t \t \t \t \t \t \t  '   `-\\  \\-' ./|\\\\ `'-.\n";
    std::cout<< "\t \t \t \t \t \t \t \t  .-----,| |--'/ | \\\\    \\\n";
    std::cout<< "\t \t \t \t \t \t \t \t /     | | |  /  |  \\\\   /\n";
    std::cout<< "\t \t \t \t \t \t \t \t       | | | /   |   \\`'`\n";
    std::cout<< "\t \t \t \t \t \t \t \t       ) | | |  /\\   /                   ,,\n";
    std::cout<< "\t \t \t \t \t \t \t \t      / ;| | `-'  `'`                     \\`-.\n";
    std::cout<< "\t \t \t \t \t \t \t \t     / / | |                   ,'          |   \\\n";
    std::cout<< "\t \t \t \t \t \t \t \t \\_.' /  ; |            .-'-.,','           |  '.\n";
    std::cout<< "\t \t \t \t \t \t \t \t ,_.' ,-'  ;               ) )/|            |   |\n";
    std::cout<< "\t \t \t \t \t \t \t \t    ,'      \\         __ ,''  ''`.         _|   |--,.\n";
    std::cout<< "\t \t \t \t \t \t \t \t   /         `.     ,',./ .-.-.   \\.--.  / _) _,'--. )\n";
    std::cout<< "\t \t \t \t \t \t \t \t  |       ---..`.._| |   /__|__\\   ,-. \\ |`/ ( `--. _)\n";
    std::cout<< "\t \t \t \t \t \t \t \t  \\            `.  '\\_;  ((o|o))    ,' | \\<_, `'-, )\n";
    std::cout<< "\t \t \t \t \t \t \t \t   `.            \\   ,-''.--'--.''-. _,'  \\    __. '.\n";
    std::cout<< "\t \t \t \t \t \t \t \t     `.          |  /    \\     /    \\     /  ,'   `._)\n";
    std::cout<< "\t \t \t \t \t \t \t \t       \\        ,' | .'   `-.-'   '. |   /  /\n";
    std::cout<< "\t \t \t \t \t \t \t \t       /      _,   | |      |      | |../  ,'\n";
    std::cout<< "\t \t \t \t \t \t \t \t     .'  /'--'._,,_ \\ \\     |     / .'    /\n";
    std::cout<< "\t \t \t \t \t \t \t \t    (_._ '''-._    `.\\ `.   |   ,' ,' ___/\n";
    std::cout<< "\t \t \t \t \t \t \t \t        `-.    \\      '. `'-'-'` ,'-'`\n";
    std::cout<< "\t \t \t \t \t \t \t \t           \\ `. \\      |`'-...-'`\n";
    std::cout<< "\t \t \t \t \t \t \t \t           | ; \\ |    /  /\n";
    std::cout<< "\t \t \t \t \t \t \t \t           | | | /  ,' .'     ____\n";
    std::cout<< "\t \t \t \t \t \t \t \t          (_/.'-'  (   `.   ,'    `'._\n";
    std::cout<< "\t \t \t \t \t \t \t \t                     `.  `-'     --.. )\n";
    std::cout<< "\t \t \t \t \t \t \t \t                       `.       .--. `\\\n";
    std::cout<< "\t \t \t \t \t \t \t \t                         `.__.-\\ \\  `\\_)\n";
    std::cout<< "\t \t \t \t \t \t \t \t                               ( /\n";
    std::cout<< "\t \t \t \t \t \t \t \t                                `\n";
}

int info_iniciais(int NumA, int NumB, int NumC, int LimiteMax){
    const int Multiplicacao = NumA*NumB*NumC, Soma = NumA + NumB + NumC;
    system("cls");
    
    image();

    std::cout<< "\t \t \t \t \t \t \t \t-------------------- Adivinhe os numeros --------------------";
    std::cout<< std::endl;
    std::cout<< "\t \t \t \t \t \t \t \t|                                                           "<<std::endl;
    std::cout<< "\t \t \t \t \t \t \t \t|     + Temos 3 numeros [1 ate "<<LimiteMax<<"]."<<std::endl;
    std::cout<< "\t \t \t \t \t \t \t \t|     + O valor da Multiplicacao deles eh: "<<Multiplicacao << std::endl;
    std::cout<< "\t \t \t \t \t \t \t \t|     + O valor da Soma deles eh: "<<Soma<<std::endl;
    std::cout<< "\t \t \t \t \t \t \t \t|                                                           "<<std::endl;

    return 0;
}

void  verificando_acerto(int PlayerGuess, int NumA, int NumB, int NumC, int AcertouA, int AcertouB, int AcertouC) {
    //static bool AcertouPrimeiro, AcertouSegundo , AcertouTerceiro;

    if (PlayerGuess == NumA && AcertouA != NumA) {
        //AcertouPrimeiro = true;
        AcertouA = NumA;
    }else if (PlayerGuess == NumB && AcertouB != NumB) {
        //AcertouSegundo = true;
        AcertouB = NumB;
    }else if (PlayerGuess == NumC && AcertouC != NumC) {
        //AcertouTerceiro = true;
        AcertouC = NumC;
    }
}//Deprecated

void PlayGame(){
    int NumA, NumB, NumC, LimiteMax;
    srand(time(NULL));  /* srand(time(NULL)) objetiva inicializar o gerador de números aleatórios
                         com o valor da função time(NULL). Este por sua vez, é calculado
                        como sendo o total de segundos passados desde 1 de janeiro de 1970
                        até a data atual.
                        Desta forma, a cada execução o valor da "semente" será diferente.
                        */
    // gerando valores aleatórios na faixa de 0 a 29 (somando +1, vira de 1 a 30)
    LimiteMax = 30;
    NumA = 1 + rand()%LimiteMax;
    NumB = 1 + rand()%LimiteMax;
    NumC = 1 + rand()%LimiteMax;

    info_iniciais(NumA,NumB,NumC,LimiteMax);

    int PlayerGuessA, PlayerGuessB, PlayerGuessC;
    int SavePlayerGuessA, SavePlayerGuessB, SavePlayerGuessC;
    int AcertouA=0, AcertouB=0, AcertouC=0;

    bool acertou = false;
    bool AcertouPrimeiro = false, AcertouSegundo = false, AcertouTerceiro = false;
    int Chances = 3;
    int NumTentativa = 1;

    while (acertou == false && Chances>0)
    {   
        std::cout<<"\t \t \t \t \t \t \t \t|     > Quais sao os 3 numeros?\n";
        std::cout<<"\t \t \t \t \t \t \t \t|     >Num 1: ";
        std::cin>>PlayerGuessA;
        SavePlayerGuessA = PlayerGuessA;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout<<"\t \t \t \t \t \t \t \t|     >Num 2: ";
        std::cin>>PlayerGuessB;
        SavePlayerGuessB = PlayerGuessB;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout<<"\t \t \t \t \t \t \t \t|     >Num 3: ";
        std::cin>>PlayerGuessC;
        SavePlayerGuessC = PlayerGuessC;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

        //verificando_acerto(PlayerGuessA, NumA, NumB, NumC, AcertouA, AcertouB, AcertouC);
        //verificando_acerto(PlayerGuessB, NumA, NumB, NumC, AcertouA, AcertouB, AcertouC);
        //verificando_acerto(PlayerGuessC, NumA, NumB, NumC, AcertouA, AcertouB, AcertouC);

        if (PlayerGuessA == NumA && AcertouPrimeiro == false) {
            AcertouA = NumA;
            AcertouPrimeiro = true;
        }else if(PlayerGuessA == NumB && AcertouSegundo == false){
            AcertouB = NumB;
            AcertouSegundo = true;
        }else if(PlayerGuessA == NumC && AcertouTerceiro == false){
            AcertouC = NumC;
            AcertouTerceiro= true;
        } 

        if(PlayerGuessB == NumA && AcertouPrimeiro == false){
            AcertouA = NumA;
            AcertouPrimeiro = true;
        }else if(PlayerGuessB == NumB && AcertouSegundo == false){
            AcertouB = NumB;
            AcertouSegundo = true;
        }else if(PlayerGuessB == NumC && AcertouTerceiro == false){
            AcertouC = NumC;
            AcertouTerceiro= true;
        }
        if(PlayerGuessC == NumA && AcertouPrimeiro == false){
            AcertouA = NumA;
            AcertouPrimeiro = true;
        }else if(PlayerGuessC == NumB && AcertouSegundo == false){
            AcertouB = NumB;
            AcertouSegundo = true;
        } else if(PlayerGuessC == NumC && AcertouTerceiro == false){
            AcertouC = NumC;
            AcertouTerceiro= true;
        }     
        
        if (AcertouA != 0 && AcertouB != 0 && AcertouC != 0) {
            std::cout << "\t \t \t \t \t \t \t \t|                                                           " << std::endl;
            std::cout << "\t \t \t \t \t \t \t \t|     (: Acertou!\n";
            acertou = true;
        }
        else {
            Chances--;
            info_iniciais(NumA, NumB, NumC, LimiteMax);
            tentativas(NumTentativa, SavePlayerGuessA, SavePlayerGuessB, SavePlayerGuessC, AcertouA, AcertouB, AcertouC);
            NumTentativa++;
            std::cout << "\t \t \t \t \t \t \t \t|                                                           " << std::endl;
            std::cout << "\t \t \t \t \t \t \t \t|     X Errado, voce tem mais " << Chances << " Chances\n";
        }

        /*if ((PlayerGuessA == NumA) || (PlayerGuessA == NumB) || (PlayerGuessA == NumC)) {
            if((PlayerGuessB == NumA) || (PlayerGuessB == NumB) || (PlayerGuessB == NumC)){
                if((PlayerGuessC == NumA) || (PlayerGuessC == NumB) || (PlayerGuessC == NumC)){
                    std::cout<< "\t \t \t \t \t \t \t \t|                                                           "<<std::endl;
                    std::cout<<"\t \t \t \t \t \t \t \t|     (: Acertou!\n";
                    acertou = true;
                }else{
                    Chances--;
                    info_iniciais(NumA,NumB,NumC,LimiteMax);
                    tentativas(NumTentativa,SavePlayerGuessA,SavePlayerGuessB,SavePlayerGuessC, AcertouA, AcertouB, AcertouC);
                    NumTentativa++;
                    std::cout<< "\t \t \t \t \t \t \t \t|                                                           "<<std::endl;
                    std::cout<<"\t \t \t \t \t \t \t \t|     X Errado, voce tem mais "<< Chances << " Chances\n";
                }
            }else{
                Chances--;
                info_iniciais(NumA,NumB,NumC,LimiteMax);
                tentativas(NumTentativa,SavePlayerGuessA,SavePlayerGuessB,SavePlayerGuessC, AcertouA, AcertouB, AcertouC);
                NumTentativa++;
                std::cout<< "\t \t \t \t \t \t \t \t|                                                           "<<std::endl;
                std::cout<<"\t \t \t \t \t \t \t \t|     X Errado, voce tem mais "<< Chances << " Chances\n";
            }
        }else{
            Chances--;
            info_iniciais(NumA,NumB,NumC,LimiteMax);
            tentativas(NumTentativa,SavePlayerGuessA,SavePlayerGuessB,SavePlayerGuessC, AcertouA, AcertouB, AcertouC);
            NumTentativa++;
            std::cout<< "\t \t \t \t \t \t \t \t|                                                           "<<std::endl;
            std::cout<<"\t \t \t \t \t \t \t \t|     X Errado, voce tem mais "<< Chances << " Chances\n";
        }*/
        
    }
    std::cout<<"\t \t \t \t \t \t \t \t|     Gabarito: "<<NumA<<", "<<NumB<<", "<<NumC<<std::endl;
}

int main(){
   bool Sair = false;
   char Digito_Sair[1];

   while(Sair == false) {
       PlayGame();
       std::cout << "\t \t \t \t \t \t \t \t|     >Deseja Jogar Novamente? [s/n] ";
       std::cin >> Digito_Sair[0];
       std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
       if (Digito_Sair[0] == 's' || Digito_Sair[0] == 'S') {
           Sair = false;
       }
       else {
           Sair = true;
       }
   }
    return 0;
}   